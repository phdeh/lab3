
#ifndef LAB_TASK_HPP
#define LAB_TASK_HPP

#define TaskId uint16_t
#define TaskPriority uint16_t
#define TaskTime uint16_t
#define TaskDuration uint16_t

#include <cstdint>

namespace lab
{
    enum TaskType
    {
        T0,
        T1,
        T2
    };

    TaskId id = 1;

    class Task
    {
    public:
        const TaskId id;
        const TaskTime time;
        const TaskType type;
        const TaskDuration durationTime;

        Task(TaskTime taskTime,
             TaskType taskType,
             TaskDuration durationTime) :
                time(taskTime),
                type(taskType),
                durationTime(durationTime),
                id(lab::id++)
        {
        };

        Task(const Task &other) :
                time(other.time),
                type(other.type),
                durationTime(other.durationTime),
                id(other.id)
        {
        };

    private:
    };
}

#endif //LAB_TASK_HPP
