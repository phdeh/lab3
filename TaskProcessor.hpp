
#ifndef LAB_TASKPROCESSOR_HPP
#define LAB_TASKPROCESSOR_HPP

#include <istream>

#include "Task.hpp"

namespace lab
{
    class TaskProcessor
    {
    public:
        bool isBusy()
        {
            return currtime != 0;
        }

        bool isNotBusy()
        {
            return !isBusy();
        }

        bool tick()
        {
            if (currtime > 0)
                currtime--;
            return isBusy();
        }

        void handle(Task& task)
        {
            this->task = &task;
            currtime = task.time;
        }

        void print()
        {
            if (isBusy())
                std::cout << "[" << task->time
                          << ", " << task->durationTime
                          << "]";
        }

    private:
        Task *task = nullptr;
        TaskTime currtime = 0;
    };
}

#endif //LAB_TASKPROCESSOR_HPP
