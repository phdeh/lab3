
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <random>
#include <sstream>

#include "Array.hpp"
#include "TaskManager.hpp"

using namespace lab;
using namespace std;

enum TaskParserState
{
    WAITING_TASK_TIME,
    TASK_TIME,
    TASK_TIME_SEPARATOR,
    WAITING_TASK_TYPE_T,
    TASK_TYPE,
    TASK_TYPE_SEPARATOR,
    WAITING_TASK_DURATION,
    TASK_DURATION,
    TASK_DURATION_SEPARATOR,
    DONE,
};

Array<Task *> getTasks()
{
    vector<Task *> tasks;
    ifstream in;
    in.open("table.txt");
    TaskTime ttime;
    TaskType ttype;
    TaskDuration tdur;
    if (in.good())
    {
        string s;
        stringstream temp;
        getline(in, s);
        auto state = WAITING_TASK_TIME;
        for (auto i : {2})
            getline(in, s);
        while (getline(in, s))
        {
            for (auto c : s + '\n')
            {
                switch (state)
                {
                    case WAITING_TASK_TIME:
                    {
                        if ('0' <= c && c <= '9')
                        {
                            state = TASK_TIME;
                            temp << c;
                        } else if (c == '-')
                            state = DONE;
                        else if (c != ' ' && c != '\t')
                            throw WAITING_TASK_TIME;
                        break;
                    }
                    case TASK_TIME:
                    {
                        if ('0' <= c && c <= '9')
                            temp << c;
                        else if (c == ' ' || c == '\t' || c == '|')
                        {
                            if (c != '|')
                                state = TASK_TIME_SEPARATOR;
                            else
                                state = WAITING_TASK_TYPE_T;
                            ttime = stoi(temp.str());
                            temp.str("");
                        } else
                            throw TASK_TIME;
                        break;
                    }
                    case TASK_TIME_SEPARATOR:
                    {
                        if (c == '|')
                            state = WAITING_TASK_TYPE_T;
                        else if (c != ' ' && c != '\t')
                            throw TASK_TIME_SEPARATOR;
                        break;
                    }
                    case WAITING_TASK_TYPE_T:
                    {
                        if (c == 'T')
                            state = TASK_TYPE;
                        else if (c != ' ' && c != '\t')
                            throw WAITING_TASK_TYPE_T;
                        break;
                    }
                    case TASK_TYPE:
                    {
                        if ('0' <= c && c <= '9')
                            temp << c;
                        else if (c == ' ' || c == '\t' || c == '|')
                        {
                            if (c != '|')
                                state = TASK_TYPE_SEPARATOR;
                            else
                                state = WAITING_TASK_DURATION;
                            auto a = stoi(temp.str());
                            temp.str("");
                            auto t = sizeof(TaskType) - 1;
                            if (a < t)
                                ttype = (TaskType) a;
                            else
                                throw -1;
                        } else
                            throw TASK_TYPE;
                        break;
                    }
                    case TASK_TYPE_SEPARATOR:
                    {
                        if (c == '|')
                            state = WAITING_TASK_DURATION;
                        else if (c != ' ' && c != '\t')
                            throw TASK_TYPE_SEPARATOR;
                        break;
                    }
                    case WAITING_TASK_DURATION:
                    {
                        if ('0' <= c && c <= '9')
                        {
                            state = TASK_DURATION;
                            temp << c;
                        } else if (c != ' ' && c != '\t')
                            throw WAITING_TASK_TIME;
                        break;
                    }
                    case TASK_DURATION:
                    {
                        if ('0' <= c && c <= '9')
                            temp << c;
                        else if (c == ' ' || c == '\t' || c == '\n')
                        {
                            state = TASK_DURATION_SEPARATOR;
                            auto a = temp.str();
                            tdur = stoi(a);
                            temp.str("");
                            tasks.push_back(new Task(ttime, ttype, tdur));
                        } else
                            throw TASK_DURATION;
                        break;
                    }
                    default:
                    {

                    }
                        break;
                }

                if (state == DONE)
                    break;
                if (state == TASK_DURATION_SEPARATOR)
                {
                    state = WAITING_TASK_TIME;
                    break;
                }
            }
            if (state == DONE)
                break;
        }
    } else
    {
        random_device rd;
        mt19937 gen(rd());
        for (size_t i = 0; i < 15 + gen() % 15; i++)
        {
            ttime = gen() % 20;
            ttype = (TaskType) (gen() % (sizeof(TaskType) - 1));
            tdur = gen() % 50;
            cout << "[" << ttime
                 << ", " << ttype
                 << ", " << tdur
                 << "]\n";
            tasks.push_back(new Task(
                    ttime,
                    ttype,
                    tdur
            ));
        }
    }
    in.close();
    Array<Task *> arr(tasks.size());
    for (size_t i = 0; i < tasks.size(); i++)
        arr[i] = tasks[i];
    return arr;
}


int main()
{
    TaskManager tm;
    auto tasks = getTasks();
    auto left = tasks.length;
    do
    {
        for (size_t i = 0; i < tasks.length; ++i)
            if (tasks[i] && tasks[i]->durationTime == tm.getTime())
            {
                if (tm.push(*tasks[i]))
                    left--;
                else {
                    cout << "Queue overflowed!\n";
                    return 1;
                }
            }
    } while (tm.tick() || left > 0);
    for (size_t i = 0; i < tasks.length; ++i)
        delete tasks[i];
    return 0;
}