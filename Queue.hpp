

#ifndef LAB_QUEUE_HPP
#define LAB_QUEUE_HPP

#include <cstddef>
#include <iostream>
#include <string>

namespace lab
{
    enum QueueExceptions
    {
        QUEUE_IS_FULL,
        QUEUE_IS_EMPTY
    };

    template<typename T>
    class QueueElement
    {
    public:
        const T value;

        QueueElement(T value) : value(value)
        {
        }
    };

    template<typename T>
    class Queue
    {
    public:
        const size_t length;

        Queue(size_t length) : length(length),
                               members(new const QueueElement<T> *[length])
        {
            for (size_t i; i < length; ++i)
                members[i] = nullptr;
        }

        ~Queue()
        {
            for (size_t i = 0; i < length; ++i)
                if (members[i] != nullptr)
                    delete members[i];
            delete[] members;
        }

        bool isEmpty()
        {
            return members[readingHead] == nullptr;
        }

        bool isNotEmpty()
        {
            return !isEmpty();
        }

        bool isFull()
        {
            return members[writingHead] != nullptr;
        }

        bool isNotFull()
        {
            return !isFull();
        }

        void push(T value)
        {
            if (isNotFull())
            {
                members[writingHead] = new QueueElement<T>(value);
                auto h = writingHead + 1;
                if (h >= length) h -= length;
                writingHead = h;
            } else
                throw QUEUE_IS_FULL;
        }

        T pop()
        {
            if (isNotEmpty())
            {
                auto t = members[readingHead];
                members[readingHead] = nullptr;
                auto v = t->value;
                auto h = readingHead + 1;
                if (h >= length) h -= length;
                readingHead = h;
                delete t;
                return v;
            } else
                throw QUEUE_IS_EMPTY;
        }

        T peek()
        {
            if (isNotEmpty())
            {
                return members[readingHead]->value;
            } else
                throw QUEUE_IS_EMPTY;
        }

        void each(void (*eacher)(T))
        {
            auto i = readingHead;
            while (i != writingHead) {
                eacher(members[i]->value);
                i++;
                if (i == length)
                    i = 0;
            }
        }

    private:
        QueueElement<T> const **members;
        size_t readingHead = 0;
        size_t writingHead = 0;
    };
}

#endif //LAB_QUEUE_HPP
