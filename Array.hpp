#ifndef PROJECT_ARRAY_HPP
#define PROJECT_ARRAY_HPP

#include <iostream>
#include <functional>

namespace lab
{
    enum
    {
        OUT_OF_BOUNDS_ERROR
    };

    template<class T>
    class Array
    {
    public:
        const size_t length;

        Array(size_t length) : length(length)
        {
            arr = new T[length];
        };

        Array(Array<T> &other) : length(other.length)
        {
            arr = new T[length];
            for (size_t i = 0; i < length; i++)
                arr[i] = other[i];
        }

        ~Array()
        {
            delete[] arr;
        }

        bool isEmpty()
        {
            for (size_t i = 0; i < length; i++)
                if (arr != nullptr)
                    return false;
            return true;
        }

        T &operator[](size_t i)
        {
            if (0 <= i && i < length)
                return arr[i];
            else
                throw OUT_OF_BOUNDS_ERROR;
        };
    private:
        T *arr;
    };
}

#endif //PROJECT_ARRAY_HPP
