

#ifndef LAB_STACK_HPP
#define LAB_STACK_HPP

#include <cstddef>

namespace lab
{
    template<typename T>
    class StackElement
    {
    public:
        const T value;
        StackElement<T> const *prev;

        StackElement(T value, StackElement<T> *prev) :
                value(value), prev(prev)
        {
        }
    };

    template<typename T>
    class Stack
    {
    public:
        ~Stack()
        {
            auto root = this->root;
            this->root = nullptr;
            while (root)
            {
                root = const_cast<StackElement<T> *>(root->prev);
                delete root;
            }
        }

        bool isEmpty()
        {
            return root == nullptr;
        }

        bool isNotEmpty()
        {
            return !isEmpty();
        }

        size_t getLength()
        {
            return length;
        }

        void push(T value)
        {
            length++;
            root = new StackElement<T>(value, root);
        }

        T pop()
        {
            length--;
            auto r = root;
            auto t = r->value;
            root = const_cast<StackElement<T>*>(r->prev);
            delete r;
            return t;
        }

        T peek()
        {
            return root->value;
        }

        void each(void (*eacher)(T))
        {
            auto r = root;
            while (r != nullptr) {
                eacher(r->value);
                r = const_cast<StackElement<T>*>(r->prev);
            }
        }

    private:
        StackElement<T> *root = nullptr;
        size_t length = 0;
    };
}

#endif //LAB_STACK_HPP
