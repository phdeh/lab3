
#ifndef LAB_TASKMANAGER_HPP
#define LAB_TASKMANAGER_HPP

#include <iostream>

#include "Task.hpp"
#include "TaskProcessor.hpp"

#include "Stack.hpp"
#include "Queue.hpp"
#include "Array.hpp"

namespace lab
{
    class TaskManager
    {
    public:
        bool push(Task task)
        {
            if (queue.isNotFull())
            {
                queue.push(task);
                return true;
            }
            return false;
        }

        bool tick()
        {
            for (size_t i = 0; i < processors.length; ++i)
                processors[i].tick();
            std::cout << "TICK " << time << ":\n";
            if (queue.isNotEmpty())
            {
                std::cout << "  Queue content: " << '\n';
                queue.each([](Task it)
                           {
                               std::cout << "       ["
                                         << it.time
                                         << ", "
                                         << it.type
                                         << ", "
                                         << it.durationTime
                                         << "]\n";
                           });
            }
            if (stack.isNotEmpty())
            {
                std::cout << "Stack content: " << '\n';
                stack.each([](Task it)
                           {
                               std::cout << "       ["
                                         << it.time
                                         << ", "
                                         << it.type
                                         << ", "
                                         << it.durationTime
                                         << "]\n";
                           });
            }
            if (queue.isNotEmpty())
            {
                auto task = queue.pop();
                if (processors[task.type].isNotBusy())
                    processors[task.type].handle(task);
                else
                    stack.push(task);
            }
            if (stack.isNotEmpty())
            {
                auto task = stack.peek();
                if (processors[task.type].isNotBusy())
                {
                    processors[task.type].handle(task);
                    stack.pop();
                }
            }
            std::cout << "  Processors:\n";
            for (size_t i = 0; i < processors.length; ++i)
            {
                std::cout << "       P" << i << ": " <<
                          (processors[i].isBusy() ? "busy " : "idle ");
                processors[i].print();
                std::cout << '\n';
            }
            time++;
            std::cout << "_________________\n";
            for (size_t i = 0; i < processors.length; ++i)
                if (processors[i].isBusy())
                    return true;
            return queue.isNotEmpty() || stack.isNotEmpty();
        }

        TaskTime getTime()
        {
            return time;
        }

    private:
        Queue<Task> queue = Queue<Task>(10);
        Stack<Task> stack;
        Array<TaskProcessor> processors =
                Array<TaskProcessor>(sizeof(TaskType) - 1);
        TaskTime time = 0;
    };
}

#endif //LAB_TASKMANAGER_HPP
